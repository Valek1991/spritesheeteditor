﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ToolbarController : MonoBehaviour {

    [SerializeField]
    private InputField columnsInputField;
    [SerializeField]
    private Toggle trimToggle;
    [SerializeField]
    private Text atlasSizeLabel;
    [SerializeField]
    private Button openButton;
    [SerializeField]
    private Button saveButton;
    [SerializeField]
    private Button createButton;

    public int GetColumnsCount() {

        if (columnsInputField.text != "") {

            return System.Convert.ToInt32(columnsInputField.text);
        }

        return 0;
    }

    public bool IsTrimming() {

        return trimToggle.isOn;
    }

    public void SetAtlasSize(string size) {

        atlasSizeLabel.text = size;
    }

    public void SetToolBarIteractable(bool isIteractable) {

        columnsInputField.interactable = isIteractable;
        trimToggle.interactable = isIteractable;
        openButton.interactable = isIteractable;
        saveButton.interactable = isIteractable;
        createButton.interactable = isIteractable;
    }

}
