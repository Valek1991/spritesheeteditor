﻿using UnityEngine;
using System.Collections.Generic;
using ReadyBuilder;
using SkywardRay.FileBrowser;

public class EditorManager : LocalSingletonBehaviour<EditorManager> {

    private SkywardFileBrowser skywardFileBrowser;
    private Texture2D atlas;

    [SerializeField]
    private GameObject loadingBar;
    [SerializeField]
    private SkywardFileBrowser skywardFileBrowserPrefab;

    public delegate void LoadTexturesHandler (List<Texture2D> textures);
    private LoadTexturesHandler loadTexturesHandler = delegate { };

    public override void DoAwake() {

        base.DoAwake();
        skywardFileBrowser = Instantiate<SkywardFileBrowser>(skywardFileBrowserPrefab);
        skywardFileBrowser.Settings.KeepBrowserInMemoryWhenClosed = false;
        skywardFileBrowser.Settings.RequireFileExtensionInSaveMode = true;
    }

    public void LoadTextures() {

        skywardFileBrowser.OpenFile(Application.dataPath, OnLoadTextures, new string[] { ".png" });
    }

    public void SaveAtlas(Texture2D atlas) {

        this.atlas = atlas;
        skywardFileBrowser.SaveFile(Application.dataPath, OnSaveAtlas, new string[] { ".png" });
    }

    public void CloseEditor() {

        Application.Quit();
    }

    public void EnableLoadingBar(bool isEnabled) {

        loadingBar.SetActive(isEnabled);
    }

    private void OnLoadTextures(string [] paths) {

        List<Texture2D> textures = new List<Texture2D>();
        foreach (string path in paths) {

            textures.Add(FileUtils.ReadPNG(path));
        }

        loadTexturesHandler(textures);
    }

    private void OnSaveAtlas(string [] path) {

        FileUtils.WritePNG(atlas, path[0]);
    }

    public void AddLoadTexturesHandler(LoadTexturesHandler loadTexturesHandler) {

        this.loadTexturesHandler += loadTexturesHandler;
    }

    public void RemoveLoadTexturesHandler(LoadTexturesHandler loadTexturesHandler) {

        this.loadTexturesHandler -= loadTexturesHandler;
    }

}
