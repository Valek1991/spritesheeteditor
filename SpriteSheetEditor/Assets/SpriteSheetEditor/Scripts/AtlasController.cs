﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class AtlasController : MonoBehaviour {

    private List<Texture2D> loadedTextures;
    private List<TextureEntity> croppedTextures = new List<TextureEntity>();
    private Texture2D atlas;
    private bool isStoppedAlphaResearch = false;
    private int maxWidth = 0;
    private int maxHeight = 0;
    private int startPixelPosX = 0;
    private int endPixelPosX = 0;
    private int startPixelPosY = 0;
    private int endPixelPosY = 0;
    private int columns = 4;
    private int rows = 1;
    private int columnCounter = 0;

    [SerializeField]
    private ToolbarController toolbarController;

    protected void Start() {

        EditorManager.Instance.AddLoadTexturesHandler(OnLoadTextures);
    }

    protected void OnApplicationQuit() {

        EditorManager.Instance.RemoveLoadTexturesHandler(OnLoadTextures);
    }

    public void OnLoadTextures(List<Texture2D> textures) {

        loadedTextures = textures;

        if ((loadedTextures != null) && (loadedTextures.Count > 0)) {

            CreateAtlas();
        }
    }

    public void CreateAtlas() {

        columns = toolbarController.GetColumnsCount();

        if ((columns > 0) && (loadedTextures != null) && (loadedTextures.Count > 0)) {

            RemoveSprite();
            croppedTextures.Clear();
            if (toolbarController.IsTrimming()) {

                StartCoroutine(GenerateTrimAtlas());
            } else {

                StartCoroutine(GenerateAtlas());
            }
        }
    }

    public void SaveAtlas() {

        if (atlas != null) {

            EditorManager.Instance.SaveAtlas(atlas);
        }
    }

    private void FindMaxTextureSize(Texture2D texture
        , int firstPassStartPixelPos, int firstPassEndPixelPos, int secondPassStartPixelPos, int secondPassEndPixelPos
        , bool isFindingWidth, out int startPixelPos, out int endPixelPos) {

        startPixelPos = 0;
        endPixelPos = 0;
        isStoppedAlphaResearch = false;

        int x = 0;
        int y = 0;

        //find left or bottom edge
        for (int i = firstPassStartPixelPos; i <= firstPassEndPixelPos; ++i) {

            for (int j = secondPassStartPixelPos; j <= secondPassEndPixelPos; ++j) {

                if (isFindingWidth) {

                    x = i;
                    y = j;
                } else {

                    x = j;
                    y = i;
                }

                if (texture.GetPixel(x, y).a > 0) {

                    if (isFindingWidth) {

                        startPixelPos = x;
                    } else {

                        startPixelPos = y;
                    }

                    startPixelPos = (startPixelPos - 2) < 0 ? startPixelPos : (startPixelPos - 2);
                    isStoppedAlphaResearch = true;
                    break;
                }
            }

            if (isStoppedAlphaResearch) {

                isStoppedAlphaResearch = false;
                break;
            }
        }

        //find right or top edge
        for (int i = firstPassEndPixelPos; i >= firstPassStartPixelPos; --i) {

            for (int j = secondPassEndPixelPos; j >= secondPassStartPixelPos; --j) {

                if (isFindingWidth) {

                    x = i;
                    y = j;
                } else {

                    x = j;
                    y = i;
                }

                if (texture.GetPixel(x, y).a > 0) {

                    if (isFindingWidth) {

                        endPixelPos = x;
                    } else {

                        endPixelPos = y;
                    }

                    endPixelPos = (endPixelPos + 2) > firstPassEndPixelPos
                        ? endPixelPos : (endPixelPos + 2);
                    isStoppedAlphaResearch = true;
                    break;
                }
            }

            if (isStoppedAlphaResearch) {

                isStoppedAlphaResearch = false;
                break;
            }
        }
    }

    private void CreateSprite(Texture2D texture) {

        Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height)
            , new Vector2(0.5F, 0.5F), 1.0f);
        GameObject gameObject = new GameObject("Atlas");
        Image image = gameObject.AddComponent<Image>();
        image.sprite = sprite;
        image.rectTransform.sizeDelta = new Vector2(texture.width, texture.height);
        gameObject.transform.SetParent(transform);
        gameObject.transform.localPosition = new Vector2(0.0F, 0.0F);
        gameObject.transform.localScale = new Vector3(1.0F, 1.0F, 1.0F);
        toolbarController.SetAtlasSize(string.Format("{0}x{1}", texture.width, texture.height));
    }

    private void RemoveSprite() {

        for (int i = 0; i < transform.childCount; ++i) {

            Destroy(transform.GetChild(i).gameObject);
        }
    }

    private IEnumerator GenerateAtlas() {

        EditorManager.Instance.EnableLoadingBar(true);
        toolbarController.SetToolBarIteractable(false);

        maxWidth = loadedTextures[0].width;
        maxHeight = loadedTextures[0].height;

        foreach (Texture2D texture in loadedTextures) {

            croppedTextures.Add(new TextureEntity(texture, 0, 0, 0, 0));
        }

        yield return StartCoroutine(MakeAtlasGrid());

        EditorManager.Instance.EnableLoadingBar(false);
        toolbarController.SetToolBarIteractable(true);
    }

    private IEnumerator GenerateTrimAtlas() {

        EditorManager.Instance.EnableLoadingBar(true);
        toolbarController.SetToolBarIteractable(false);

        maxWidth = 0;
        maxHeight = 0;

        //find texture's max width and height
        foreach (Texture2D texture in loadedTextures) {

            FindMaxTextureSize(texture, 0, texture.width, 0, texture.height, true, out startPixelPosX, out endPixelPosX);
            FindMaxTextureSize(texture, 0, texture.height, 0, texture.width, false, out startPixelPosY, out endPixelPosY);

            croppedTextures.Add(new TextureEntity(texture, startPixelPosX, endPixelPosX, startPixelPosY, endPixelPosY));

            yield return null;
        }

        //find general min and max x, y positions
        startPixelPosX = croppedTextures.Min<TextureEntity>(x => x.StartPixelPosX);
        endPixelPosX = croppedTextures.Max<TextureEntity>(x => x.EndPixelPosX);
        startPixelPosY = croppedTextures.Min<TextureEntity>(x => x.StartPixelPosY);
        endPixelPosY = croppedTextures.Max<TextureEntity>(x => x.EndPixelPosY);

        maxWidth = endPixelPosX - startPixelPosX;
        maxHeight = endPixelPosY - startPixelPosY;

        if ((maxWidth % 2) != 0) {

            ++maxWidth;
        }

        if ((maxHeight % 2) != 0) {

            ++maxHeight;
        }

        //create cropped textures
        foreach (TextureEntity textureEntity in croppedTextures) {

            maxWidth = (maxWidth + startPixelPosX) > textureEntity.Texture.width
                ? (textureEntity.Texture.width - startPixelPosX) : maxWidth;
            maxHeight = (maxHeight + startPixelPosY) > textureEntity.Texture.height
                ? (textureEntity.Texture.height - startPixelPosY) : maxHeight;
            Color[] pixels = textureEntity.Texture.GetPixels(
                startPixelPosX, startPixelPosY
                , maxWidth
                , maxHeight);

            textureEntity.Texture = new Texture2D(maxWidth, maxHeight);
            textureEntity.Texture.SetPixels(0, 0
                , maxWidth
                , maxHeight, pixels);

            yield return null;
        }

        //create atlas from cropped textures
        yield return StartCoroutine(MakeAtlasGrid());

        EditorManager.Instance.EnableLoadingBar(false);
        toolbarController.SetToolBarIteractable(true);
    }

    private IEnumerator MakeAtlasGrid() {

        if (croppedTextures.Count < columns) {

            columns = croppedTextures.Count;
        }

        rows = (int)Mathf.Ceil((float)croppedTextures.Count / (float)columns);
        columnCounter = columns;

        atlas = new Texture2D(maxWidth * columns, maxHeight * rows);
        startPixelPosX = 0;
        startPixelPosY = maxHeight * (rows - 1);

        //At start, make atlas transparent
        Color[] alphaPixels = new Color[(maxWidth * columns) * (maxHeight * rows)];
        for (int i = 0; i < alphaPixels.Length; ++i) {

            alphaPixels[i] = new Color(1, 1, 1, 0);
        }
        atlas.SetPixels(alphaPixels);

        //Add texture into atlas
        for (int i = 0; i < croppedTextures.Count; ++i) {

            atlas.SetPixels(startPixelPosX, startPixelPosY
                , maxWidth, maxHeight, croppedTextures[i].Texture.GetPixels());
            atlas.Apply();
            startPixelPosX += maxWidth;
            --columnCounter;

            if (columnCounter == 0) {

                startPixelPosY -= maxHeight;
                startPixelPosX = 0;
                columnCounter = columns;
            }

            yield return null;
        }

        CreateSprite(atlas);
    }

    private class TextureEntity {

        public Texture2D Texture { get; set; }
        public int StartPixelPosX { get; set; }
        public int EndPixelPosX { get; set; }
        public int StartPixelPosY { get; set; }
        public int EndPixelPosY { get; set; }

        public TextureEntity(Texture2D texture, int startPixelPosX, int endPixelPosX, int startPixelPosY, int endPixelPosY) {

            Texture = texture;
            StartPixelPosX = startPixelPosX;
            EndPixelPosX = endPixelPosX;
            StartPixelPosY = startPixelPosY;
            EndPixelPosY = endPixelPosY;
        }

    }

}
