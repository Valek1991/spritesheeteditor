﻿using UnityEngine;
using System.IO;
using System.Collections.Generic;

public static class FileUtils {

    public static Texture2D ReadPNG(string path) {

        byte[] bytes = File.ReadAllBytes(path);
        Texture2D texture = new Texture2D(1, 1);
        texture.LoadImage(bytes);

        return texture;
    }

    public static void WritePNG(Texture2D texture, string path) {

        File.WriteAllBytes(path, texture.EncodeToPNG());
    }

}
